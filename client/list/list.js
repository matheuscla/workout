Template.list.helpers({
	tasklist: function() {
		return Task.find({});
	},

	'sum' : function(){
		var sum = 0;
		var cursor = Task.find({});
		cursor.forEach(function(task){
		var	time =parseInt(task.time);

			sum = sum + time;

		});
		return sum;
	}

});

Template.list.events({
	"click button" : function(e, template) {
		var task = this;
		Task.remove({_id : task._id });
	}
});
