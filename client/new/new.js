Template.new.events({
  "submit form" : function(e, template){
    e.preventDefault();

    var InputName = $("#taskName");
    var taskName = InputName.val();

    var InputTime = $("#taskTime");
    var taskTime = InputTime.val();

    var InputDate = $("#taskDate");
    var taskDate = InputDate.val();

    var item = {name: taskName, time: taskTime , date: taskDate};
    Task.insert(item);
    InputName.val();

  }
});
